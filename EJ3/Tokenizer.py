import argparse
import os
import re

def argsParse():
    parser = argparse.ArgumentParser(description='Tokenizer')
    parser.add_argument('-i', "--InputDirectory", required=True, help="Directory where the colection is")
    parser.add_argument('-e', "--EmptyWords", required=False, help="File with the stop words to delete. If is not specified the stop words will not be deleted")
    parser.add_argument('-r', "--Regex", required=True, choices=['True', 'False'], help="Defines whether to use Chemistry regex or not")

    args = parser.parse_args()
    return args.InputDirectory, args.EmptyWords, args.Regex

def translate(to_translate):
    tabin = u'áéíóúüâêîôûöëäï'
    tabout = u'aeiouuaeiouoeai'
    tabin = [ord(char) for char in tabin]
    translate_table = dict(zip(tabin, tabout))
    return to_translate.translate(translate_table)

def getFormulas(line):
    formulas = re.findall(r'[1-9]?(?:(?:[A-Z][a-z]*[1-9]?)+[\-\+]?(?: ?\([a-z]+\) ?)?(?: \+ [1-9]?(?:[A-Z][a-z]*[1-9]?[\-\+]?)+(?: ?\([a-z]+\) ?)?)*) => [1-9]?(?:[A-Z][a-z]*[1-9]?)+[\-\+]?(?: ?\([a-z]+\) ?)?(?: ?\+ ?[1-9]?(?:[A-Z][a-z]*[1-9]?)+[\-\+]?(?: ?\([a-z]+\) ?)?)*', line)
    newLine = re.sub(r'[1-9]?(?:(?:[A-Z][a-z]*[1-9]?)+[\-\+]?(?: ?\([a-z]+\) ?)?(?: \+ [1-9]?(?:[A-Z][a-z]*[1-9]?[\-\+]?)+(?: ?\([a-z]+\) ?)?)*) => [1-9]?(?:[A-Z][a-z]*[1-9]?)+[\-\+]?(?: ?\([a-z]+\) ?)?(?: ?\+ ?[1-9]?(?:[A-Z][a-z]*[1-9]?)+[\-\+]?(?: ?\([a-z]+\) ?)?)*', '', line)
    return formulas, newLine

def Tokenizar(line, Regex):
    list_Tokens = []
    newLine = line
    if Regex == 'True':
        Formulas, newLine = getFormulas(newLine)
        list_Tokens.extend(Formulas)
    tokens = re.split(r'[\t ]', line)
    for token in tokens:
        token = token.strip()
        token = token.lower()
        token = translate(token)
        token = re.sub('[◄►•«»;:-\[\]!\"\$%&\(\)=,·\\ \'\`´|\{\}\-\@/°\+\*\“¿›>¡!\\\/_]', '', token)
        token = re.sub('[\.\?]$', '', token)
        if len(token) > 0:
            list_Tokens.append(token)
    return list_Tokens

def sacar_palabras_vacias(lista_tokens, lista_vacias):
    list_terms = []
    for token in lista_tokens:
        if token not in lista_vacias:
            list_terms.append(token)
    return list_terms


if __name__ == "__main__":
    termsCF = {}
    termsDF = {}

    ColectionDirectory, EmptyWords, Regex = argsParse()
    LT = 0
    lista_vacias = []
    if not(EmptyWords is None):
        with open(EmptyWords, 'r') as fv:
            for line in fv.readlines():
                lista_vacias.append(line.replace("\n", ""))

    
    for f in os.listdir(ColectionDirectory):
        DocumentTerms = []
        #print(f)
        with open(os.path.join(ColectionDirectory, f), 'r',encoding='UTF-8') as File:
            TD = 0
            TermsD = 0
            archivo = File.read()
            
            lista_tokens = Tokenizar(archivo.replace("\n", " "), Regex)
            list_terms = lista_tokens
            
            if lista_vacias:
                list_terms = sacar_palabras_vacias(lista_tokens, lista_vacias)

            for term in list_terms:
                if len(term) > LT:
                    LT = len(term)
                if term in termsCF:
                    termsCF[term] += 1
                else:
                    termsCF[term] = 1
                if not(term in DocumentTerms):
                    DocumentTerms.append(term)


        for term in DocumentTerms:
            if term in termsDF:
                termsDF[term] += 1
            else:
                termsDF[term] = 1

    with open('terminos.txt', 'w', encoding='UTF-8') as terminos:
        terminos.write(("{:"+str(LT)+"} {:20} {:20}\n").format("Termino", "Collection Frequency", "Document Frequency"))
        termsCFord = sorted(termsCF)
        for term in termsCFord:
            terminos.write(("{:"+str(LT)+"} {:20} {:20}\n").format(term, termsCF[term], termsDF[term]))

    pass