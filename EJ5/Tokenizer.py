import argparse
import os
import re
from nltk.stem.porter import PorterStemmer
from nltk.stem.lancaster import LancasterStemmer
import time

def argsParse():
    parser = argparse.ArgumentParser(description='Tokenizer')
    parser.add_argument('-i', "--InputDirectory", required=True, help="Directory where the colection is")
    parser.add_argument('-e', "--EmptyWords", required=False, help="File with the stop words to delete. If is not specified the stop words will not be deleted")
    parser.add_argument('-s','--Stem',required=True, choices=['Lancaster', 'Porter', 'Both'], help="Which stemmer algorithm to use or use both. If uses both creates another file comparing for each term the two stems")


    args = parser.parse_args()
    return args.InputDirectory, args.EmptyWords, args.Stem

def translate(to_translate):
    tabin = u'áéíóúüâêîôûöëäï'
    tabout = u'aeiouuaeiouoeai'
    tabin = [ord(char) for char in tabin]
    translate_table = dict(zip(tabin, tabout))
    return to_translate.translate(translate_table)

def Tokenizar(line):

    list_Tokens = []
    tokens = re.split(r'[\t\' ]', line)
    for token in tokens:
        token = token.strip()
        token = token.lower()
        token = translate(token)
        token = re.sub('[◄►•«»;:-\[\]!\"\$%&\(\)=,·\\ \`´|\{\}\-\@/°\+\*\“¿›>¡!\\\/\_\”#\.\?]', '', token)
        if len(token) > 0:
            list_Tokens.append(token)
    return list_Tokens

def sacar_palabras_vacias(lista_tokens, lista_vacias):
    list_terms = []
    for token in lista_tokens:
        if token not in lista_vacias:
            list_terms.append(token)
    return list_terms

def stemming(list_terms, stem):
    list_steams = []
    stemmer = None
    if stem == 'Lancaster':
        stemmer = LancasterStemmer()
        for term in list_terms:
            stem = stemmer.stem(term)
            list_steams.append(stem)
        return list_steams
    elif stem == 'Porter':
        stemmer = PorterStemmer()
        for term in list_terms:
            stem = stemmer.stem(term)
            list_steams.append(stem)
        return list_steams
    elif stem == 'Both':
        list_porter = []
        porter = PorterStemmer()
        stemmer = LancasterStemmer()
        with open("stemmers comparisson.txt",'w',encoding='UTF-8') as cs:
            cs.write("{:40} {:30} {:30}\n".format("Termino", "Porter stem", "Lancaster stem"))
            for term in list_terms:
                stem = stemmer.stem(term)
                pstem = porter.stem(term)
                cs.write("{:40} {:30} {:30}\n".format(term, pstem, stem))
                list_steams.append(stem)
                list_porter.append(pstem)
        return list_steams, list_porter

if __name__ == "__main__":
    t1 = time.time()
    termsCF = {}
    termsDF = {}
    termsPCF = {}
    termsPDF = {}
    ColectionDirectory, EmptyWords, Stem = argsParse()
    LT = 0
    lista_vacias = []
    if not(EmptyWords is None):
        with open(EmptyWords, 'r', encoding='UTF-8') as fv:
            for line in fv.readlines():
                lista_vacias.append(line.replace("\n", ""))

    
    for f in os.listdir(ColectionDirectory):
        DocumentTerms = []
        DocumentTermsP = []
        #print(f)
        with open(os.path.join(ColectionDirectory, f), 'r',encoding='UTF-8') as File:
            TD = 0
            TermsD = 0
            archivo = File.read()
            
            lista_tokens = Tokenizar(archivo.replace("\n", " "))
            list_terms = lista_tokens
            
            if lista_vacias:
                list_terms = sacar_palabras_vacias(lista_tokens, lista_vacias)
            list_termsP = []
            if Stem == 'Both':
                list_terms, list_termsP = stemming(list_terms, Stem)
            else:
                list_terms = stemming(list_terms, Stem)

            for term in list_terms:
                if len(term) > LT:
                    LT = len(term)
                if term in termsCF:
                    termsCF[term] += 1
                else:
                    termsCF[term] = 1
                if not(term in DocumentTerms):
                    DocumentTerms.append(term)
            
            if list_termsP:
                for term in list_termsP:
                    if len(term) > LT:
                        LT = len(term)
                    if term in termsPCF:
                        termsPCF[term] += 1
                    else:
                        termsPCF[term] = 1
                    if not(term in DocumentTermsP):
                        DocumentTermsP.append(term)

        for term in DocumentTerms:
            if term in termsDF:
                termsDF[term] += 1
            else:
                termsDF[term] = 1
        for term in DocumentTermsP:
            if term in termsPDF:
                termsPDF[term] += 1
            else:
                termsPDF[term] = 1
    st = "{:"+str(LT)+"} {:20} {:20}\n"
    with open('terminos.txt', 'w', encoding='UTF-8') as terminos:
        
        terminos.write(st.format("Termino", "Collection Frequency", "Document Frequency"))
        termsCFord = sorted(termsCF)
        for term in termsCFord:
            terminos.write(st.format(term, termsCF[term], termsDF[term]))
    if termsPCF:
        with open('terminos-porter.txt', 'w', encoding='UTF-8') as terminos:
            terminos.write(st.format("Termino", "Collection Frequency", "Document Frequency"))
            termsCFord = sorted(termsPCF)
            for term in termsCFord:
                terminos.write(st.format(term, termsPCF[term], termsPDF[term]))
    t2 = time.time()
    print("Using {} stemmer algorithm(s) lasted {:.2f} seconds".format(Stem, t2-t1))
    pass