import argparse
import os
import re
from nltk.stem.snowball import SpanishStemmer

def argsParse():
    parser = argparse.ArgumentParser(description='Tokenizer')
    parser.add_argument('-i', "--InputDirectory", required=True, help="Directory where the colection is")
    parser.add_argument('-e', "--EmptyWords", required=False, help="File with the stop words to delete. If is not specified the stop words will not be deleted")
    parser.add_argument('-s','--Stem',required=True, choices=['True', 'False'], help="Defines whether to use or not steammer")


    args = parser.parse_args()
    return args.InputDirectory, args.EmptyWords, args.Stem

def translate(to_translate):
    tabin = u'áéíóúüâêîôûöëäï'
    tabout = u'aeiouuaeiouoeai'
    tabin = [ord(char) for char in tabin]
    translate_table = dict(zip(tabin, tabout))
    return to_translate.translate(translate_table)

def Tokenizar(line):
    list_Tokens = []
    tokens = line.split(" ")
    for token in tokens:
        token = token.strip()
        token = token.lower()
        token = translate(token)
        token = re.sub('[◄►•«»;:-\[\]!\"\$%&\(\)=,·\\ \'\`´|\{\}\-\@/°\+\*\“¿›>¡!\\\/\_\”]', '', token)
        token = re.sub('[\.\?]', '', token)
        if len(token) > 0:
            list_Tokens.append(token)
    return list_Tokens

def sacar_palabras_vacias(lista_tokens, lista_vacias):
    list_terms = []
    for token in lista_tokens:
        if token not in lista_vacias:
            list_terms.append(token)
    return list_terms

def stemming(list_terms):
    list_steams = []
    stemmer = SpanishStemmer()
    for term in list_terms:
        stem = stemmer.stem(term)
        list_steams.append(stem)
    return list_steams



if __name__ == "__main__":
    termsCF = {}
    termsDF = {}

    DP = 0
    TT = 0
    TermsTotales = 0
    sumLen = 0
    CTSD = 0
    CTermsSD = 0
    CTLD = 0
    CTermsLD = 0
    ColectionDirectory, EmptyWords, Stem = argsParse()
    LT = 0
    lista_vacias = []
    if not(EmptyWords is None):
        with open(EmptyWords, 'r') as fv:
            for line in fv.readlines():
                lista_vacias.append(line.replace("\n", ""))

    
    for f in os.listdir(ColectionDirectory):
        DocumentTerms = []
        #print(f)
        with open(os.path.join(ColectionDirectory, f), 'r',encoding='UTF-8') as File:
            TD = 0
            TermsD = 0
            archivo = File.read()
            
            lista_tokens = Tokenizar(archivo.replace("\n", " "))
            list_terms = lista_tokens
            
            if lista_vacias:
                list_terms = sacar_palabras_vacias(lista_tokens, lista_vacias)
            
            if Stem == 'True':
                list_terms = stemming(list_terms)
            

            
            TT += len(lista_tokens)
            TermsTotales += len(list_terms)
            TD += len(lista_tokens)
            TermsD += len(list_terms)

            for term in list_terms:
                sumLen += len(term)
                if len(term) > LT:
                    LT = len(term)
                if term in termsCF:
                    termsCF[term] += 1
                else:
                    termsCF[term] = 1
                if not(term in DocumentTerms):
                    DocumentTerms.append(term)

        for term in DocumentTerms:
            if term in termsDF:
                termsDF[term] += 1
            else:
                termsDF[term] = 1
    uniqueterms = 0
    with open('terminos.txt', 'w', encoding='UTF-8') as terminos:
        terminos.write(("{:"+str(LT)+"} {:20} {:20}\n").format("Termino", "Collection Frequency", "Document Frequency"))
        termsCFord = sorted(termsCF)
        for term in termsCFord:
            if (termsCF[term] == 1):
                uniqueterms += 1
            terminos.write(("{:"+str(LT)+"} {:20} {:20}\n").format(term, termsCF[term], termsDF[term]))

    pass