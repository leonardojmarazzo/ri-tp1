import argparse
import os
import re
import numpy as np

def argsParse():
    parser = argparse.ArgumentParser(description='Tokenizer')
    parser.add_argument('-i', "--InputDirectory", required=True, help="Directory where the colection is")
    parser.add_argument('-e', "--EmptyWords", required=False, help="File with the stop words to delete. If is not specified the stop words will not be deleted")
    parser.add_argument('-r', "--Regex", required=True, choices=['True', 'False'], help="Defines whether to use Chemistry regex or not")

    args = parser.parse_args()
    return args.InputDirectory, args.EmptyWords, args.Regex

def translate(to_translate):
    tabin = u'áéíóúüâêîôûöëäï'
    tabout = u'aeiouuaeiouoeai'
    tabin = [ord(char) for char in tabin]
    translate_table = dict(zip(tabin, tabout))
    return to_translate.translate(translate_table)

def getAbreviaturas(line):
    abv = re.findall(r'\b[A-Z]\.?[A-z\.?]+\b', line)
    newLine = re.sub(r'\b[A-Z]\.?[A-z\.?]+\b', '', line)
    return abv, newLine

def getEmails(line):
    emails = re.findall(r'\b\w+\@\w+(?:\.\w+)+\b', line)
    newLine = re.sub(r'\b\w+@\w+(?:\.\w+)+\b', '', line)
    return emails, newLine

def getUrls(line):
    urls = re.findall(r'\b\w+(?:\.\w+)+\b', line)
    newLine = re.sub(r'\b\w+(?:\.\w+)+\b', '', line)
    return urls, newLine

def getTelefonos(line):
    tels = re.findall(r'\b(?:\+?[0-9]{1,3}[- ]?)?(?:[0-9]{2,5}[- ]?)?[0-9-]{6,11}\b', line)
    newLine = re.sub(r'\b(?:\+?[0-9]{1,3}[- ]?)?(?:[0-9]{2,5}[- ]?)?[0-9-]{6,11}\b', '', line)
    return tels, newLine

def getNumeros(line):
    nums = re.findall(r'\b[0-9](?:[\., ][0-9]{3})*[\.,][0-9]*\b', line)
    newLine = re.sub(r'\b[0-9](?:[\., ][0-9]{3})*[\.,][0-9]*\b', '', line)
    return nums, newLine

def getFechas(line):
    dates = re.findall(r'\b\d{1,2}[-\/]\d{1,2}[-\/]\d{2}(?:\d{2})?\b', line)
    newLine = re.sub(r'\b\d{1,2}[-\/]\d{1,2}[-\/]\d{2}(?:\d{2})?\b', '', line)
    return dates, newLine

def getNombres(line):
    nombres = re.findall(r'\b(?:[A-Z][a-z]+ ){2,}\b', line)
    newLine = re.sub(r'\b(?:[A-Z][a-z]+){2,}\b', '', line)
    return nombres, newLine

def Tokenizar(line, Regex):
    list_Tokens = []
    newLine = line
    if Regex == 'True':
        abreviaturas, newLine = getAbreviaturas(newLine)
        mails, newLine = getEmails(newLine)
        URLs, newLine = getUrls(newLine)
        tels, newLine = getTelefonos(newLine)
        nums, newLine = getNumeros(newLine)
        fechas, newLine = getFechas(newLine)
        nombres, newLine = getNombres(newLine)

        list_Tokens.extend(abreviaturas)
        list_Tokens.extend(mails)
        list_Tokens.extend(URLs)
        list_Tokens.extend(tels)
        list_Tokens.extend(nums)
        list_Tokens.extend(fechas)
        list_Tokens.extend(nombres)

    tokens = newLine.split(" ")
    for token in tokens:
        token = token.strip()
        token = token.lower()
        token = translate(token)
        token = re.sub('[◄►•«»;:-\[\]!\"\$%&\(\)=,·\\ \'\`´|\{\}\-\@/°\+\*\“¿›>¡!\\\/_]', '', token)
        token = re.sub('[\.\?]', '', token)
        if len(token) > 0:
            list_Tokens.append(token)
    return list_Tokens

def sacar_palabras_vacias(lista_tokens, lista_vacias):
    list_terms = []
    for token in lista_tokens:
        if token not in lista_vacias:
            list_terms.append(token)
    return list_terms


if __name__ == "__main__":
    termsCF = {}
    termsDF = {}
    DP = 0
    TT = 0
    TermsTotales = 0
    sumLen = 0
    CTSD = 0
    CTermsSD = 0
    CTLD = 0
    CTermsLD = 0
    ColectionDirectory, EmptyWords, Regex = argsParse()
    LT = 0
    lista_vacias = []
    if not(EmptyWords is None):
        with open(EmptyWords, 'r') as fv:
            for line in fv.readlines():
                lista_vacias.append(line.replace("\n", ""))

    
    for f in os.listdir(ColectionDirectory):
        DocumentTerms = []
        #print(f)
        with open(os.path.join(ColectionDirectory, f), 'r',encoding='UTF-8') as File:
            TD = 0
            TermsD = 0
            archivo = File.read()
            
            lista_tokens = Tokenizar(archivo.replace("\n", " "),Regex)
            list_terms = lista_tokens
            
            if lista_vacias:
                list_terms = sacar_palabras_vacias(lista_tokens, lista_vacias)
            
            TT += len(lista_tokens)
            TermsTotales += len(list_terms)
            TD += len(lista_tokens)
            TermsD += len(list_terms)

            for term in list_terms:
                sumLen += len(term)
                if len(term) > LT:
                    LT = len(term)
                if term in termsCF:
                    termsCF[term] += 1
                else:
                    termsCF[term] = 1
                if not(term in DocumentTerms):
                    DocumentTerms.append(term)
            if CTSD > TD or CTSD == 0:
                CTSD = TD
                CTermsSD = TermsD
            if CTLD < TD or CTLD == 0:
                CTLD = TD
                CTermsLD = TermsD
        
        DP += 1
        for term in DocumentTerms:
            if term in termsDF:
                termsDF[term] += 1
            else:
                termsDF[term] = 1
    uniqueterms = 0
    with open('terminos.txt', 'w', encoding='UTF-8') as terminos:
        terminos.write(("{:"+str(LT)+"} {:20} {:20}\n").format("Termino", "Collection Frequency", "Document Frequency"))
        termsCFord = sorted(termsCF)
        for term in termsCFord:
            if (termsCF[term] == 1):
                uniqueterms += 1
            terminos.write(("{:"+str(LT)+"} {:20} {:20}\n").format(term, termsCF[term], termsDF[term]))
    with open("estadisticas.txt", "w", encoding='UTF-8') as estadisticas:
        estadisticas.write("1) Cantidad de documentos procesados: {}\n".format(DP))
        estadisticas.write("2) Cantidad de tokens y terminos extraídos: {} {}\n".format(TT, TermsTotales))
        estadisticas.write("3) Promedio de tokens y términos de los documento: {:.2f} {:.2f}\n".format(TT/DP, TermsTotales/DP))
        estadisticas.write("4) Largo promedio de un término: {:.2f}\n".format(sumLen/TermsTotales))
        estadisticas.write("5) Cantidad de tokens y términos del documento más corto y del más largo: {} {} {} {}\n".format(CTSD, CTermsSD, CTLD, CTermsLD))
        estadisticas.write("6) Cantidad de términos que aparecen sólo 1 vez en la colección: {}\n".format(uniqueterms))
    with open("frecuencias.txt", 'w', encoding='UTF-8') as frecuencias:
        frecuencias.write("La lista de los 10 tárminos más frecuentes y su CF\n")
        top10more = sorted(termsCF, key=termsCF.get, reverse=True)[:10]
        top10less = sorted(termsCF, key=termsCF.get,)[:10]
        for term in top10more:
            frecuencias.write("{:20} {:20}\n".format(term, termsCF[term]))
        frecuencias.write("La lista de los 10 tárminos menos frecuentes y su CF\n")
        for term in top10less:
            frecuencias.write("{:20} {:20}\n".format(term, termsCF[term]))
    pass