import argparse
import os
import numpy as np
from langdetect import detect

def argsParse():
    parser = argparse.ArgumentParser(description="Lang detection")
    parser.add_argument("-m", "--Method", required=True, choices=['SingleChar', 'CharPairs'], help="Select which method to use. There are two methods available single characters distribution detection or pair of characters distribution.")
    parser.add_argument("-t", "--Training", required=True, help="Directory where the training files are. The file's names must be the languaje.")
    parser.add_argument("-i", "--Input", required=True, help="The input file in which detect each line languaje")
    parser.add_argument('-s', "--Solution", required=True, help="The solution file to compare the results.")
    parser.add_argument("-o", "--Output", required=True, help="File to save the output")

    args = parser.parse_args()
    return args.Method, args.Training, args.Input, args.Solution, args.Output


def TrainSingleChar(TrainingDir):
    models = {}
    for fname in os.listdir(TrainingDir):
        with open(os.path.join(TrainingDir,fname)) as f:
            model = {}
            for line in f.readlines():
                line = line.replace("\n", "")
                for char in line:
                    if char in model:
                        model[char] += 1
                    else:
                        model[char] = 1
            models[fname] = model
    return models

def TrainingPairsChar(TrainingDir):
    models = {}
    for fname in os.listdir(TrainingDir):
        with open(os.path.join(TrainingDir,fname)) as f:
            model = {}
            for line in f.readlines():
                line = line.replace("\n", "")
                for i in range(len(line)):
                    chars = line[i:i+2]
                    if len(chars) == 1:
                        chars += " "
                    if chars in model:
                        model[chars] += 1
                    else:
                        model[chars] = 1
            models[fname] = model
    return models

def MakeComparable(model1, model2):
    keys_Model1 = np.array(list(model1.keys()))
    keys_Model2 = np.array(list(model2.keys()))
    uniques = np.unique(np.concatenate((keys_Model1, keys_Model2)))
    sorted_model1 = {}
    sorted_model2 = {}
    for char in uniques:
        if char in model1:
            sorted_model1[char] = model1[char]
        else:
            sorted_model1[char] = 0
        if char in model2:
            sorted_model2[char] = model2[char]
        else:
            sorted_model2[char] = 0
    return sorted_model1, sorted_model2
        
def detectLang(models, line_model):
    corrs = {}
    probLang = {}
    for lang in models:
        modelLang = models[lang]
        CompModelLang, CompLine_model = MakeComparable(modelLang, line_model)
        probLang[lang] = np.corrcoef([CompLine_model.get(x,0) for x in CompLine_model.keys()],[CompModelLang.get(x,0) for x in CompModelLang.keys()])[1][0]
    return max(probLang, key=probLang.get)


def SingleCharDetection(Training, TestFile, Solution, Output):
    models = TrainSingleChar(Training)
    with open(TestFile) as test:
        count = 0
        countcorrects = 0
        sol = open(Solution)
        out = open(Output, 'w', encoding="UTF-8")
        st = "{:10} {:15} {:15} {:15} {:10}\n".format("Line n°", "Detected", "LangDetect", "Correct", "Assetion")
        out.write(st)
        for line in test.readlines():
            count += 1
            line = line.replace("\n", "")
            line_model = {}
            for char in line:
                if char in line_model:
                    line_model[char] += 1
                else:
                    line_model[char] = 1
            lang = detectLang(models, line_model)
            solution = sol.readline().replace("\n", " ").split(" ")[1]
            correct = ""
            if (lang.upper() == solution.upper()):
                countcorrects += 1
                correct = "Correct"
            else:
                correct = "Incorrect"
            langDetected = detect(line)
            st = "Line {:<5} {:15} {:15} {:15} {:10}\n".format(count, lang, langDetected, solution, correct)
            out.write(st)
        st = "Assetion percentage: {:.2f}%\n".format(((countcorrects/count)*100))
        out.write(st)
    pass

def CharPairsDetection(Training, TestFile, Solution, Output):
    models = TrainingPairsChar(Training)
    with open(TestFile) as test:
        count = 0
        countcorrects = 0
        sol = open(Solution)
        out = open(Output, 'w', encoding="UTF-8")
        st = "{:10} {:15} {:15} {:15} {:10}\n".format("Line n°", "Detected", "LangDetect", "Correct", "Assetion")
        out.write(st)
        for line in test.readlines():
            count += 1
            line = line.replace("\n", "")
            line_model = {}
            for i in range(len(line)):
                char = line[i:i+2]
                if len(char) == 1:
                    char += " "
                if char in line_model:
                    line_model[char] += 1
                else:
                    line_model[char] = 1
            lang = detectLang(models, line_model)
            solution = sol.readline().replace("\n", " ").split(" ")[1]
            correct = ""
            if (lang.upper() == solution.upper()):
                countcorrects += 1
                correct = "Correct"
            else:
                correct = "Incorrect"
            langDetected = detect(line)
            st = "Line {:<5} {:15} {:15} {:15} {:10}\n".format(count, lang, langDetected, solution, correct)
            out.write(st)
        st = "Assetion percentage: {:.2f}%\n".format(((countcorrects/count)*100))
        out.write(st)
    pass


if __name__ == "__main__":
    Method, Training, Test, Solution, Output = argsParse()
    if Method == 'SingleChar':
        SingleCharDetection(Training, Test, Solution, Output)
    else:
        CharPairsDetection(Training, Test, Solution, Output)
    pass
