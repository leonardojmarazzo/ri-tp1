# Trabajo Practico Análisis de Texto

### 1. Escriba un programa que realice análisis léxico sobre la colección RI-tknz-data. El programa debe recibir como parámetros el directorio donde se encuentran los documentos y un argumento que indica si se deben eliminar las palabras vacías (y en tal caso, el nombre del archivo que las contiene)

Para la realizacion de este ejercicio se creo el script Tokenizer.py dentro de la carpeta EJ1.

A continuacion se muestran los parametros requeridos por el script.
```
PS C:\Users\Leonardo Marazzo\Documents\Universidad\RI\TP1\EJ1> python .\Tokenizer.py -h
usage: Tokenizer.py [-h] -i INPUTDIRECTORY [-e EMPTYWORDS]

Tokenizer

optional arguments:
  -h, --help            show this help message and exit
  -i INPUTDIRECTORY, --InputDirectory INPUTDIRECTORY
                        Directory where the colection is
  -e EMPTYWORDS, --EmptyWords EMPTYWORDS
                        File with the stop words to delete. If is not
                        specified the stop words will not be deleted
```

Ejemplo de utilizacion:

```
PS C:\Users\Leonardo Marazzo\Documents\Universidad\RI\TP1\EJ1> python .\Tokenizer.py -i '..\Colecciones\RI-tknz-data\' -e '..\StopWords\StopWords.txt'
```

Al ejecutarlo se crean en el directorio donde se ejecute los siguientes archivos:

- Terminos.txt: Este archivo contiene los terminos extraidos, su document frequency y su collection fequency.
- Estadisticas.txt: Este archivo contiene los datos descriptos a continuacion:
    1) Cantidad de documentos procesados
    2) Cantidad de tokens y terminos extraídos
    3) Promedio de tokens y términos de los documento
    4) Largo promedio de un término
    5) Cantidad de tokens y términos del documento más corto y del más largo
    6) Cantidad de términos que aparecen sólo 1 vez en la colección
- Frequencias.txt: Este archivo contiene los 10 terminos más frecuentes y los 10 menos frecuentes.

### 2. Tomando como base el programa anterior, escriba un segundo Tokenizer que implemente los criterios del artículo de Grefenstette y Tapanainen para definir qué es una “palabra” (o término) y cómo tratar números y signos de puntuación

Este programa agruega al programa anterior las siguientes funciones:

- getAbreviaturas(line): Obtiene las abreviaturas en sus diferentes formatos que se encuentran en el parametro line.

- getEmails(line): Obtiene los emails que se encuentran en el parametro line.
 
- getUrls(line): Obtiene las urls en formato subdominios(uno o varios).dominio.top-level-domain que se encuentran en el parametro line.

- getTelefonos(line): Obtiene los numeros de telefono que se encuentran en el parametro line. Pueden presentarse en los siguientes formatos (con o sin guiones):
    - +54 (0)11 4510-1100 / +54 (0)2323 423979
    - 54 (0)11 4510-1100 / 54 (0)2323 423979
    - (0)11 4510-1100 / (0)2323 423979
    - 4510-1100 / 423979

- getNumeros(line): Obtiene los numeros que no son telefonicos que se encuentran en el parametro line.

- getFechas(line): Obtiene las fechas que se encuentran en el parametro line. Los formatos posibles son dd/mm/yyyy, d/m/yy, dd-mm-yyyy y d-m-yy

- getNombres(line): Obtiene los nombres propios que se encuentran en el parametro line.


A continuacion se muestran los parametros requeridos por el script.
```
PS C:\Users\Leonardo Marazzo\Documents\Universidad\RI\TP1\EJ2> python .\Tokenizer.py -h
usage: Tokenizer.py [-h] -i INPUTDIRECTORY [-e EMPTYWORDS] -r {True,False}

Tokenizer

optional arguments:
  -h, --help            show this help message and exit
  -i INPUTDIRECTORY, --InputDirectory INPUTDIRECTORY
                        Directory where the colection is
  -e EMPTYWORDS, --EmptyWords EMPTYWORDS
                        File with the stop words to delete. If is not
                        specified the stop words will not be deleted
  -r {True,False}, --Regex {True,False}
                        Defines whether to use Chemistry regex or not
```

Se agrega el parametro Regex con motivo de utilizar el mismo script para la comparación de los resultados.

Ejemplo de utilizacion:

```
PS C:\Users\Leonardo Marazzo\Documents\Universidad\RI\TP1\EJ2> python .\Tokenizer.py -i '..\Colecciones\RI-tknz-data\' -e '..\StopWords\StopWords.txt' -r True
```



Al ejecutarlo genera la misma informacion que el script anterior.

### 3. Repita el procesamiento del ejercicio 1 utilizando la colección RI-tknz-qm. Verifique los resultados e indique las reglas que debería modificar para que el tokenizer responda al dominio del problema

Al utilizar la mecionada colección aparecieron tokens que no corresponden al dominio y los correspondientes no aparecen. Para ajustarlo se debia agregar la siguiente expresion regular.

[1-9]?(?:(?:[A-Z][a-z]*[1-9]?)+[\-\+]?(?: ?\([a-z]+\) ?)?(?: \+ [1-9]?(?:[A-Z][a-z]*[1-9]?[\-\+]?)+(?: ?\([a-z]+\) ?)?)*) => [1-9]?(?:[A-Z][a-z]*[1-9]?)+[\-\+]?(?: ?\([a-z]+\) ?)?(?: ?\+ ?[1-9]?(?:[A-Z][a-z]*[1-9]?)+[\-\+]?(?: ?\([a-z]+\) ?)?)*

La cual obtiene las formulas quimicas que se encuentran en los archivos de la coleccion.

A continuacion se muestran los parametros requeridos por el script.
```
PS C:\Users\Leonardo Marazzo\Documents\Universidad\RI\TP1\EJ3> python.exe .\Tokenizer.py -h
usage: Tokenizer.py [-h] -i INPUTDIRECTORY [-e EMPTYWORDS] -r {True,False}

Tokenizer

optional arguments:
  -h, --help            show this help message and exit
  -i INPUTDIRECTORY, --InputDirectory INPUTDIRECTORY
                        Directory where the colection is
  -e EMPTYWORDS, --EmptyWords EMPTYWORDS
                        File with the stop words to delete. If is not
                        specified the stop words will not be deleted
  -r {True,False}, --Regex {True,False}
                        Defines whether to use Chemistry regex or not
```

Se agrega el parametro Regex con motivo de utilizar el mismo script para la comparación de los resultados.

Ejemplo de utilizacion:

```
PS C:\Users\Leonardo Marazzo\Documents\Universidad\RI\TP1\EJ3> python .\Tokenizer.py -i '..\Colecciones\RI-tknz-data\' -e '..\StopWords\StopWords.txt' -r True
```

### 4. A partir del programa del ejercicio 1, incluya un proceso de stemming. Luego de modificar su programa, corra nuevamente el proceso del ejercicio 1 y analice los cambios en la olección. ¿Qué implica este resultado? Busque ejemplos de pares de términos que tienen la misma raíz pero que el stemmer los trató diferente y términos que son diferentes y se los trató igual.

Al aplicar el proceso de stemming el programa busca la raiz de un termino antes de agregarlo a la lista. Esto reduce considerablemente la cantidad de terminos a indexar. Pero puede generar problemas ya que puede ser que dos terminos diferentes sean asignados a la misma raiz y viceversa. A continuacion se muestra un ejemplo de cada caso.
- Costos y costa son asignados a la misma raiz.
- Carri y Carril son asignadas como palabras diferentes.

A continuacion se muestran los parametros requeridos por el script.
```
PS C:\Users\Leonardo Marazzo\Documents\Universidad\RI\TP1\EJ4> python .\Tokenizer.py -h
usage: Tokenizer.py [-h] -i INPUTDIRECTORY [-e EMPTYWORDS] -s {True,False}

Tokenizer

optional arguments:
  -h, --help            show this help message and exit
  -i INPUTDIRECTORY, --InputDirectory INPUTDIRECTORY
                        Directory where the colection is
  -e EMPTYWORDS, --EmptyWords EMPTYWORDS
                        File with the stop words to delete. If is not
                        specified the stop words will not be deleted
  -s {True,False}, --Stem {True,False}
                        Defines whether to use or not steammer
```

Se agrega el parametro Stem con motivo de utilizar el mismo script para la comparación de los resultados.

Ejemplo de utilizacion:

```
PS C:\Users\Leonardo Marazzo\Documents\Universidad\RI\TP1\EJ4> python .\Tokenizer.py -i '..\Colecciones\RI-tknz-data\' -e '..\StopWords\StopWords.txt' -s True
```

### 5. A Sobre la colección CISI, ejecute los stemmers de Porter y Lancaster provistos en el módulo nltk.stem. Compare: cantidad de tokens resultantes, resultado 1 a 1 y tiempo de ejecución para toda la colección. ¿Qué conclusiones puede obtener de la ejecuci´on de uno y otro?

En la resolucion de este ejercicio en la carpeta EJ5 se agregan 3 archivos. Dos que contienen los terminos finales obtenidos con cada stemmer y otro con la comparacion de los dos stemmers. El stemmer de Lancaster obtiene casi 1000 terminos menos. Sobre esto se pueden mencionar las mismas ventajas y desventajas que en el ejercicio anterior.

A continuacion se muestran los parametros requeridos por el script.
```
PS C:\Users\Leonardo Marazzo\Documents\Universidad\RI\TP1\EJ5> python .\Tokenizer.py -h
usage: Tokenizer.py [-h] -i INPUTDIRECTORY [-e EMPTYWORDS] -s
                    {Lancaster,Porter,Both}

Tokenizer

optional arguments:
  -h, --help            show this help message and exit
  -i INPUTDIRECTORY, --InputDirectory INPUTDIRECTORY
                        Directory where the colection is
  -e EMPTYWORDS, --EmptyWords EMPTYWORDS
                        File with the stop words to delete. If is not
                        specified the stop words will not be deleted
  -s {Lancaster,Porter,Both}, --Stem {Lancaster,Porter,Both}
                        Which stemmer algorithm to use or use both. If uses
                        both creates another file comparing for each term the
                        two stems
```

El parametro stem permite seleccionar uno de los dos stemmers o realizar una comparacion entre ambos utilizando el valor "Both".

Ejemplo de utilizacion:

```
PS C:\Users\Leonardo Marazzo\Documents\Universidad\RI\TP1\EJ5> python .\Tokenizer.py -i '..\Colecciones\CISI\' -e '..\StopWords\StopWords-en.txt' -s Both
```

### 6. Escriba un programa que realice la identificacin del lenguaje de un texto a partir de un conjunto de entrenamiento. Pruebe dos métodos sencillos:
### a) Uno basado en la distribución de la frecuencia de las letras.
### b) El segundo, basado en calcular la probabilidad de que una letra x preceda a una y (calcule una matriz de probabilidades con todas las combinaciones).
### Compare los resultados contra el módulo Python langdetect y la solucin provista.

En la realizacion de este ejercicio se puede ver que el segundo metodo obtiene mucha más precisión que el primero. En la carpeta EJ6 se pueden encontrar dos archivos de texto los cuales tienen las comparaciones con la solucion y con el modulo langdetect de cada metodo.

A continuacion se muestran los parametros requeridos por el script.
```
PS C:\Users\Leonardo Marazzo\Documents\Universidad\RI\TP1\EJ6> python .\langdetection.py -h
usage: langdetection.py [-h] -m {SingleChar,CharPairs} -t TRAINING -i INPUT -s
                        SOLUTION -o OUTPUT

Lang detection

optional arguments:
  -h, --help            show this help message and exit
  -m {SingleChar,CharPairs}, --Method {SingleChar,CharPairs}
                        Select which method to use. There are two methods
                        available single characters distribution detection or
                        pair of characters distribution.
  -t TRAINING, --Training TRAINING
                        Directory where the training files are. The file's
                        names must be the languaje.
  -i INPUT, --Input INPUT
                        The input file in which detect each line languaje
  -s SOLUTION, --Solution SOLUTION
                        The solution file to compare the results.
  -o OUTPUT, --Output OUTPUT
                        File to save the output
```

Ejemplo de utilizacion:

```
PS C:\Users\Leonardo Marazzo\Documents\Universidad\RI\TP1\EJ6> python .\langdetection.py -m SingleChar -t '..\Colecciones\languageIdentificationData\training\' -i '..\Colecciones\languageIdentificationData\test' -s '..\Colecciones\languageIdentificationData\solution' - o output-pairs.txt
```
### 7. En este ejercicio se propone verificar la predicción de ley de Zipf. Para ello, descargue desde Project Gutenberg el texto del Quijote de Cervantes10 y escriba un programa que extraiga los términos y calcule sus frecuencias. Calcule la curva de ajuste utilizando la función Polyfit del módulo NymPy11. Con los datos crudos y los estimados grafique en la notebook ambas distribuciones (haga 2 gráficos, uno en escala lineal y otro en log-log). ¿Cómo se comporta la prediccin? ¿Qué conclusiones puede obtener?

Este ejercicio se encuentra resuelto en la carpeta EJ7. En la notebook de esa carpeta se puede ver que la curva ajusta muy bien en la escala log-log. mostrando que se cumple la ley de zipf.

### 8. Usando los datos del ejercicios anterior y de acuerdo a la ley de Zipf, calcule la proporción del total de términos para aquellos que tienen frecuencia f = {100, 1000, 10000}. Verifique respecto de los valores reales. ¿Qué conclusión puede obtener?

En la carpeta EJ8 se encuentra la notebook con la resolucion de este ejercicio. En la misma se puede ver que la ley de zipf predice muy bien los resultados reales. Se agrega la frecuencia 10 para mostrar con mayor claridad que a medida que baja la frecuencia aumentan los terminos que contabilizan la misma.

### 9. Para el texto del ejercicio 5 procese cada palabra en orden y calcule los pares (términos totales procesados, términos únicos). Verifique en qué medida satisface la ley de Heaps. Grafique en la notebook los ajustes variando los parámetros de la expresión.

En la notebook que se encuentra en la carpeta EJ9 se puede ver como a pesar de que los terminos unicos crecen casi linealmente a medida que crecen los tokens. La cantidad de terminos unicos es mucho menor en relacion a la cantidad de tokens totales.






